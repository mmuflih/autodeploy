package ping

import (
	"net/http"

	"github.com/mmuflih/golib/response"
	"gitlab.com/mmuflih/autodeploy/context/ping"
	"gitlab.com/mmuflih/autodeploy/http/requests"
)

type BaseHandler interface {
	Handle(w http.ResponseWriter, r *http.Request)
}

type baseH struct {
	puc ping.PingUsecase
}

func NewBaseHandler(puc ping.PingUsecase) BaseHandler {
	return &baseH{puc}
}

func (this *baseH) Handle(w http.ResponseWriter, r *http.Request) {
	req := requests.PingRequest{}
	resp, err := this.puc.Ping(req)
	response.Json(w, resp, err)
}
