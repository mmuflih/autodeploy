package provider

import (
	"gitlab.com/mmuflih/autodeploy/http/handler/auth"
	"gitlab.com/mmuflih/autodeploy/http/handler/p404"
	"gitlab.com/mmuflih/autodeploy/http/handler/ping"
)

/**
 * Created by Muhammad Muflih Kholidin
 * https://github.com/mmuflih
 * muflic.24@gmail.com
 * at: 2019-02-06 19:47
**/

func Handlers() []interface{} {
	var h []interface{}

	h = append(h, p404.NewBaseHandler)
	h = append(h, ping.NewBaseHandler)
	h = append(h, auth.NewBaseHandler)

	return h
}
