package user

import (
	"gitlab.com/mmuflih/autodeploy/config"
	"gitlab.com/mmuflih/autodeploy/domain/repository/mysql"
	"gitlab.com/mmuflih/autodeploy/http/requests"
)

/**
 * Created by Muhammad Muflih Kholidin
 * at 2020-09-26 17:46:02
 * https://github.com/mmuflih
 * muflic.24@gmail.com
 **/

type Handler interface {
	Register(req requests.RegisterRequest) (error, interface{})
	Login(req requests.LoginRequest) (error, interface{})
}

type handle struct {
	uRepo  mysql.UserRepository
	ueRepo mysql.UserEmailRepository
	upRepo mysql.UserPasswordRepository
	keys   *config.Keys
}

func NewHandler(userRepo mysql.UserRepository,
	userEmailRepo mysql.UserEmailRepository,
	userPasswordRepo mysql.UserPasswordRepository,
	keys *config.Keys) Handler {
	return &handle{userRepo, userEmailRepo, userPasswordRepo, keys}
}
